require('@/bootstrap');

window.Vue = require('vue');

import Vue from 'vue'

import Vuex from 'vuex';

import vuetify from '@/plugins/vuetify'

import App from '@/modules/app/index'

import router from '@/router'

import store from '@/store'

Vue.use(Vuex);

new Vue({
    el: '#app',
    components: {App},
    store,
    router,
    vuetify
});
