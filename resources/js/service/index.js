const ApiService = {
    get(resource, params) {
        return axios.get(`${resource}`, {params});
    },

    find(resource, slug) {
        return axios.get(`${resource}/${slug}`);
    },

    post(resource, params) {
        return axios.post(`${resource}`, params);
    },

    update(resource, slug, params) {
        return axios.put(`${resource}/${slug}`, params);
    },

    delete(resource, slug) {
        return axios.delete(`${resource}/${slug}`)
    }
};
export default ApiService;

