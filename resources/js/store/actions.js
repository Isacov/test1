export const GET_DATA_TABLE_ORDERS = "getDataTablesOrders";
export const GET_ORDER_FOR_EDIT = "getOrderForEdit";
export const GET_ALL_PARTNERS = "getAllPartners";
export const UPDATE_ORDER = "updateOrder";
export const GET_FORECAST = "getForecast";

