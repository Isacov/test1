export const SET_ORDER_SEARCH_OPTIONS = "setOrdersSearchOptions";
export const SET_ORDER_FOR_EDIT = "setOrderForEdit";
export const SET_DATA_TABLE_ORDERS = "setDataTableOrders";
export const SET_DATA_TABLE_ORDERS_TOTAL = "setDataTableOrdersTotal";
export const SET_ALL_PARTNERS = "setAllPartners";
export const SET_FORECAST = "setForecast";


