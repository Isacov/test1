import Vue from 'vue'
import Vuex from 'vuex'
import orders from "@/modules/orders/store";
import main from "@/modules/app/store";
import partners from "@/modules/partners/store"
import forecast from "@/modules/forecast/store"
Vue.use(Vuex);
export default new Vuex.Store({
    modules: {
        orders,
        main,
        partners,
        forecast
    }
});
