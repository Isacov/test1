import Vue from 'vue'
import Router from 'vue-router'

const OrderEditForm = () => import('@/modules/orders/pages/OrderEditForm');
const Orders = () => import('@/modules/orders/index');
const Forecast = () => import('@/modules/forecast/index');

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/orders',
            name: 'Orders',
            component: Orders,
            meta: {isInNav: true}
        },
        {
            path: '/forecast',
            name: 'Forecast',
            component: Forecast,
            meta: {isInNav: true}
        },
        {
            path: '/orders/:id',
            name: 'OrderEditForm',
            props: true,
            component: OrderEditForm,
            meta: {isInNav: false}
        },
    ],
})
