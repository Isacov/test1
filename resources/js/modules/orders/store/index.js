import OrderService from "@/modules/orders/service";
import {
    GET_DATA_TABLE_ORDERS,
    GET_ORDER_FOR_EDIT, UPDATE_ORDER
} from "@/store/actions";
import {
    SET_DATA_TABLE_ORDERS,
    SET_DATA_TABLE_ORDERS_TOTAL,
    SET_ORDER_FOR_EDIT,
    SET_ORDER_SEARCH_OPTIONS,
} from "@/store/mutations";

const state = {
    orders: [],
    options: {
        page: 1,
        itemsPerPage: 10,
        sortBy: ['id'],
        sortDesc: [false],
    },
    totalItems: 0,
    order: {},
};

const getters = {
    orders: (state) => state.orders,
    totalItems: (state) => state.totalItems,
    order: (state) => state.order
};

const actions = {
    async [GET_DATA_TABLE_ORDERS](context, options) {
        let order = options.sortDesc.length && options.sortDesc[0] ? 'desc' : 'asc';
        let orderBy = options.sortBy.length && options.sortBy[0] ? options.sortBy[0] : 'id';
        let search_options = {page: options.page, paginate: options.itemsPerPage, order, orderBy};
        const orders = await OrderService.getOrdersData(search_options);
        context.commit(SET_DATA_TABLE_ORDERS, orders.data.data);
        context.commit(SET_DATA_TABLE_ORDERS_TOTAL, orders.data.total);
    },
    async [GET_ORDER_FOR_EDIT](context, order_id) {
        const order = await OrderService.getOrderForEdit(order_id);
        context.commit(SET_ORDER_FOR_EDIT, order.data);

    },
    async [UPDATE_ORDER](context, order_details) {
        await OrderService.updateOrder(order_details);
    }
};

const mutations = {
    [SET_ORDER_SEARCH_OPTIONS]: (state, options) => state.options = options,
    [SET_ORDER_FOR_EDIT]: (state, order) => state.order = order,
    [SET_DATA_TABLE_ORDERS]: (state, orders) => state.orders = orders,
    [SET_DATA_TABLE_ORDERS_TOTAL]: (state, total) => state.totalItems = total,
};

export default {
    state,
    actions,
    mutations,
    getters
};


