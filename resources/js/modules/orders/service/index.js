import ApiService from '@/service';

export const OrderService = {
    getOrdersData(options) {
        return ApiService.get('/api/orders/for-table', options)
    },
    getOrderForEdit(order_id) {
        return ApiService.find('/api/orders', order_id)
    },
    updateOrder(order_details) {
        return ApiService.update('/api/orders', order_details.id, order_details.data)
    },
};

export default OrderService;
