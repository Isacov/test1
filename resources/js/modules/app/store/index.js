import router from '@/router'

const state = {
    drawer: false,
};

const getters = {
    navigation: () => router.options.routes.filter(word => word.meta.isInNav)
};

const actions = {};

const mutations = {
    setDrawer: (state, payload) => (state.drawer = payload),
    toggleDrawer: state => (state.drawer = !state.drawer),
};

export default {
    state,
    actions,
    mutations,
    getters
};


