import ForecastService from "@/modules/forecast/service";
import {GET_FORECAST} from "@/store/actions";
import {SET_FORECAST} from "@/store/mutations";

const state = {
    forecast: {}
};

const getters = {
    forecast: (state) => state.forecast
};

const actions = {
    async [GET_FORECAST](context, order_id) {
        const forecast = await ForecastService.getForecast(order_id);
        context.commit(SET_FORECAST, forecast.data);
    }
};

const mutations = {
    [SET_FORECAST]: (state, forecast) => state.forecast = forecast,
};

export default {
    state,
    actions,
    mutations,
    getters
};


