import ApiService from '@/service';

export const ForecastService = {
    getForecast(options) {
        return ApiService.get('/api/forecast', options)
    }
};

export default ForecastService;
