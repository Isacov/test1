import ApiService from '@/service';

export const PartnerService = {
    getAllPartners() {
        return ApiService.get(`/api/partners/for-select`)
    }
};

export default PartnerService;
