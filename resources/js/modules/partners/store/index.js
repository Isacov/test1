import PartnerService from "@/modules/partners/service";
import {GET_ALL_PARTNERS} from "@/store/actions";
import {SET_ALL_PARTNERS} from "@/store/mutations";

const state = {
    partners: []
};

const getters = {
    partners: (state) => state.partners
};

const actions = {
    async [GET_ALL_PARTNERS](context, order_id) {
        const partners = await PartnerService.getAllPartners(order_id);
        context.commit(SET_ALL_PARTNERS, partners.data);

    }
};

const mutations = {
    [SET_ALL_PARTNERS]: (state, partners) => state.partners = partners,
};

export default {
    state,
    actions,
    mutations,
    getters
};


