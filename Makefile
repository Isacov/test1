start: stop
	@docker-compose -f docker-compose.yml up -d 
	@make php
stop:
	@docker-compose stop
php:
	@docker-compose exec php-fpm bash
mysql:
	@docker-compose exec mysql bash
kill:
	@docker kill $$(docker ps -q)
rebuild: stop
	@docker-compose -f docker-compose.yml up -d --force-recreate --build
	@make php
