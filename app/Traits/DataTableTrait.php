<?php

namespace App\Traits;

trait DataTableTrait
{
    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeDataTableResource($query)
    {
        $paginate = request()->input('paginate');
        $orderBy = request()->input('orderBy');
        $order = request()->input('order');
        return $query->orderBy($orderBy, $order)->paginate($paginate);
    }
}
