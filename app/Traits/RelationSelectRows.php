<?php

namespace App\Traits;

trait RelationSelectRows
{
    /**
     * @param $query
     *
     * @param $relation
     * @param $select
     *
     * @return mixed
     */
    public function scopeWithSelected($query, $relation, $select)
    {
        return $query->with([$relation => static fn($q) => $q->select($select)]);
    }
}
