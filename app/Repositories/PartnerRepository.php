<?php

namespace App\Repositories;

use App\Partner;

class PartnerRepository
{
    /**
     * @var Partner
     */
    private Partner $partner;

    /**
     * PartnerRepository constructor.
     *
     * @param Partner $partner
     */
    public function __construct(Partner $partner)
    {
        $this->partner = $partner;
    }

    public function getAllForSelect()
    {
        return $this->partner->get()->map(fn($partner) => ['value' => $partner->id, 'text' => $partner->name]);
    }
}
