<?php

namespace App\Repositories;

use App\Order;
use Illuminate\Support\Facades\DB;

class OrderRepository
{
    /**
     * @var Order
     */
    private Order $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getDataTableData()
    {
        return $this->order->withSelected('products', ['name'])
            ->join('order_products', 'orders.id', 'order_products.order_id')
            ->join('partners', 'orders.partner_id', 'partners.id')
            ->select('orders.id as id', 'orders.status as status', 'partners.name as partner',
                DB::raw('SUM(order_products.price*order_products.quantity) as order_price'))
            ->groupBy('order_products.order_id')
            ->dataTableResource();
    }

    public function getOrderToEdit($id)
    {
        return $this->order
            ->withSelected('partner', ['id', 'name'])
            ->withSelected('order_products.product', ['id', 'name'])
            ->withSelected('order_products', ['price', 'quantity', 'product_id', 'order_id'])
            ->select('orders.id', 'orders.partner_id', 'orders.client_email', 'orders.status')
            ->find($id);
    }

    public function update($params, $id)
    {
        return $this->order->where('id', $id)->update($params);
    }
}
