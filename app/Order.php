<?php

namespace App;

use App\Traits\DataTableTrait;
use App\Traits\RelationSelectRows;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use DataTableTrait, RelationSelectRows;

    protected $fillable = [
        'status',
        'client_email',
        'partner_id'
    ];

    public function products() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'order_products')
            ->using(OrderProduct::class);
    }

    public function partner() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Partner::class);
    }

    public function order_products() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(OrderProduct::class);
    }
}
