<?php

namespace App\Services;

use App\Repositories\PartnerRepository;

class PartnerService
{
    /**
     * @var PartnerRepository
     */
    private PartnerRepository $partnerRepository;

    /**
     * PartnerService constructor.
     *
     * @param PartnerRepository $partnerRepository
     */
    public function __construct(PartnerRepository $partnerRepository)
    {
        $this->partnerRepository = $partnerRepository;
    }

    public function getAllForSelect()
    {
        return $this->partnerRepository->getAllForSelect();
    }
}
