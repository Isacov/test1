<?php

namespace App\Services;

use App\Repositories\OrderRepository;
use Illuminate\Http\Request;

class OrderService
{
    /**
     * @var OrderRepository
     */
    private OrderRepository $orderRepository;

    /**
     * OrderService constructor.
     *
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getDataTableData()
    {
        return $this->orderRepository->getDataTableData();
    }

    public function getOrderToEdit($id)
    {
        $order = $this->orderRepository->getOrderToEdit($id);
        $order->price = $order->order_products->sum(fn($order_products) => $order_products->price * $order_products->quantity);
        return $order;
    }

    public function update(Request $request, $id)
    {
        return $this->orderRepository->update($request, $id);
    }
}
