<?php

namespace App\Http\Controllers;

use App\OpenWeather\Services\ForecastService;
use Illuminate\Http\Request;

class ForecastController
{
    /**
     * @var ForecastService
     */
    private ForecastService $forecastService;

    public function __construct(ForecastService $forecastService)
    {
        $this->forecastService = $forecastService;
    }

    public function index(Request $request)
    {
        return $this->forecastService->getForecastForLocation($request);
    }
}
