<?php

namespace App\Http\Controllers;

use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @var OrderService
     */
    private OrderService $orderService;

    /**
     * OrderController constructor.
     *
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function allForDataTable()
    {
        $orders = $this->orderService->getDataTableData();
        return response()->json($orders, 200);
    }

    public function show($id)
    {
        $order = $this->orderService->getOrderToEdit($id);
        return response()->json($order, 200);
    }

    public function update(Request $request, $id)
    {
        $this->orderService->update($request, $id);
        return response()->json(['msg' => 'success'], 200);
    }
}
