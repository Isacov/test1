<?php

namespace App\Http\Controllers;

use App\Services\PartnerService;

class PartnerController extends Controller
{

    /**
     * @var PartnerService
     */
    private PartnerService $partnerService;


    public function __construct(PartnerService $partnerService)
    {
        $this->partnerService = $partnerService;
    }

    public function allForSelect()
    {
        $partners = $this->partnerService->getAllForSelect();
        return response()->json($partners, 200);
    }
}
