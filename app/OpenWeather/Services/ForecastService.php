<?php

namespace App\OpenWeather\Services;

use App\OpenWeather\ClientWeather;
use Illuminate\Http\Request;

class ForecastService
{
    /**
     * @var ClientWeather
     */
    private ClientWeather $clientWeather;

    public function __construct(ClientWeather $clientWeather)
    {
        $this->clientWeather = $clientWeather;
    }

    public function getForecastForLocation(Request $request)
    {
        $params = $this->getParamsForForecast($request);

        return $this->clientWeather->getWeather($params);
    }

    private function getParamsForForecast(Request $request) : array
    {
        $result = [];

        if ($request->exists('city')) {
            $result = ['q' => $request->input('city')];
        } else if ($request->exists(['lat', 'lon'])) {
            $result = [
                'lat'  => $request->input('lat'),
                'lon' => $request->input('lon')
            ];
        }

        return $result;
    }
}
