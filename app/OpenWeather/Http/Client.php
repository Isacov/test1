<?php

namespace App\OpenWeather\Http;

use GuzzleHttp\Client as GuzzleClint;
use GuzzleHttp\Exception\RequestException;

class Client
{
    /**
     * @var array
     */
    private array $payload;

    /**
     * @var GuzzleClint
     */
    private GuzzleClint $guzzleClient;

    public function __construct(GuzzleClint $guzzleClient)
    {
        $this->payload = [
            'query' => [
                'appid' => env('OPEN_WEATHER_KEY'),
                'lang'  => 'ru',
                'units' => 'metric'
            ]
        ];
        $this->guzzleClient = $guzzleClient;
    }

    public function payload() : array
    {
        return $this->payload;
    }

    public function updatePayloadParams($params) : array
    {
        return [
            'query' => array_merge($this->payload()['query'], $params)
        ];
    }

    public function get($url, $params)
    {
        $updatedPayload = $this->updatePayloadParams($params);

        try {
            return json_decode($this->guzzleClient->get($url, $updatedPayload)->getBody(), true, 512, JSON_THROW_ON_ERROR);
        } catch (RequestException $exception) {
            $response = $exception->getResponse();
            if ($response !== null) {
                $error = json_decode($response->getBody(), false, 512, JSON_THROW_ON_ERROR);
                abort($error->cod,$error->message);
            }
        }
    }
}
