<?php

namespace App\OpenWeather;

class ClientWeather
{
    // Units ('metric' or 'imperial' [default]):
    private string $units = 'metric';

    // Language setup https://openweathermap.org/current#multi:
    private string $lang = 'en';

    /**
     * @var OpenWeather
     */
    private OpenWeather $openWeather;

    public function __construct(OpenWeather $openWeather)
    {
        $this->openWeather = $openWeather;
    }

    public function getWeather($params)
    {
        $updatedParams = $this->updateSettings($params);

        return $this->openWeather->currentWeather($updatedParams);
    }

    public function updateSettings($params) : array
    {
        return array_merge($params, [
            'units' => $this->units,
            'lang'  => $this->lang
        ]);
    }
}
