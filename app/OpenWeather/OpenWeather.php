<?php

namespace App\OpenWeather;

use App\OpenWeather\Http\Client;

class OpenWeather
{
    /**
     * @var string Weather data url.
     */
    private string $weatherUrl = 'https://api.openweathermap.org/data/2.5/weather?';

    /**
     * @var Client
     */
    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function currentWeather($params)
    {
        return $this->client->get($this->getUrl(), $params);
    }

    private function getUrl() : string
    {
        return $this->weatherUrl;
    }
}
