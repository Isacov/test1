<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/partners/for-select', 'PartnerController@allForSelect');

Route::get('/forecast', 'ForecastController@index');

Route::get('/orders/for-table', 'OrderController@allForDataTable');
Route::get('/orders/{id}', 'OrderController@show');
Route::put('/orders/{id}', 'OrderController@update');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
