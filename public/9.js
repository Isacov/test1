(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _store_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/store/actions */ "./resources/js/store/actions.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'OrderEditForm',
  props: ['id'],
  created: function created() {
    this.$store.dispatch(_store_actions__WEBPACK_IMPORTED_MODULE_1__["GET_ORDER_FOR_EDIT"], this.id);
    this.$store.dispatch(_store_actions__WEBPACK_IMPORTED_MODULE_1__["GET_ALL_PARTNERS"]);
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(['partners', 'order']), {
    selectedPartner: function selectedPartner() {
      return this.order.partner.id;
    },
    orderIsNotEmpty: function orderIsNotEmpty() {
      return !_.isEmpty(this.order);
    },
    status: {
      get: function get() {
        return this.initial_status || !this.orderIsNotEmpty ? this.initial_status : this.order.status;
      },
      set: function set(val) {
        this.initial_status = val;
      }
    },
    email: {
      get: function get() {
        return this.initial_email || !this.orderIsNotEmpty ? this.initial_email : this.order.client_email;
      },
      set: function set(val) {
        this.initial_email = val;
      }
    },
    partner: {
      get: function get() {
        return this.initial_partner || !this.orderIsNotEmpty ? this.initial_partner : this.selectedPartner;
      },
      set: function set(val) {
        this.initial_partner = val;
      }
    }
  }),
  methods: {
    submit: function submit() {
      var _this = this;

      if (!this.$refs.form.validate()) {
        this.notification('error', 'Form is not valid');
        return false;
      }

      var order_details = {
        id: this.id,
        data: {
          status: this.status,
          client_email: this.email,
          partner_id: this.partner
        }
      };
      this.$store.dispatch(_store_actions__WEBPACK_IMPORTED_MODULE_1__["UPDATE_ORDER"], order_details).then(function () {
        _this.notification('success', 'Updated successfully');

        _this.$router.push('/orders');
      })["catch"](function (e) {
        _this.notification('error', "Code: ".concat(e.response.status, " Message:").concat(e.response.statusText));
      });
    },
    reset: function reset() {
      this.$refs.form.reset();
    },
    notification: function notification(type, message) {
      this.$root.$emit('notify', type, message);
    },
    updateValue: function updateValue(name, value) {
      this[name] = value;
    }
  },
  data: function data() {
    return {
      valid: true,
      initial_status: '',
      statusRules: [function (v) {
        return !!v || v === 0 || 'Status is required';
      }],
      initial_email: '',
      emailRules: [function (v) {
        return !!v || 'E-mail is required';
      }, function (v) {
        return /.+@.+\..+/.test(v) || 'E-mail must be valid';
      }],
      initial_partner: null,
      partnerRules: [function (v) {
        return !!v || 'Partner is required';
      }]
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=template&id=e13375e6&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=template&id=e13375e6& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    [
      _c(
        "v-container",
        [
          _c(
            "v-card-title",
            { staticClass: "text-center justify-center py-6" },
            [
              _c("h1", { staticClass: "font-weight-bold display-1" }, [
                _vm._v("Edit order: " + _vm._s(_vm.id))
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "v-form",
            {
              ref: "form",
              attrs: { "lazy-validation": "" },
              model: {
                value: _vm.valid,
                callback: function($$v) {
                  _vm.valid = $$v
                },
                expression: "valid"
              }
            },
            [
              _c("v-text-field", {
                attrs: { rules: _vm.statusRules, label: "Status" },
                model: {
                  value: _vm.status,
                  callback: function($$v) {
                    _vm.status = $$v
                  },
                  expression: "status"
                }
              }),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: { rules: _vm.emailRules, label: "E-mail" },
                model: {
                  value: _vm.email,
                  callback: function($$v) {
                    _vm.email = $$v
                  },
                  expression: "email"
                }
              }),
              _vm._v(" "),
              _vm.orderIsNotEmpty
                ? _c("v-select", {
                    attrs: {
                      items: _vm.partners,
                      rules: _vm.partnerRules,
                      label: "Partner",
                      value: _vm.selectedPartner
                    },
                    model: {
                      value: _vm.partner,
                      callback: function($$v) {
                        _vm.partner = $$v
                      },
                      expression: "partner"
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c(
                "v-list",
                _vm._l(_vm.order.order_products, function(
                  order_product,
                  index
                ) {
                  return _vm.orderIsNotEmpty
                    ? _c(
                        "v-list-item",
                        { key: index },
                        [
                          _c(
                            "v-list-item-content",
                            [
                              _c("v-list-item-title", [
                                _vm._v(
                                  _vm._s(order_product.quantity) +
                                    " * " +
                                    _vm._s(order_product.product.name) +
                                    "\n                        "
                                )
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    : _vm._e()
                }),
                1
              ),
              _vm._v(" "),
              _c("v-divider"),
              _vm._v(" "),
              _vm.orderIsNotEmpty
                ? _c(
                    "v-list-item",
                    [
                      _c("v-list-item-title", [
                        _vm._v("Total: " + _vm._s(_vm.order.price))
                      ])
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  staticClass: "mr-4",
                  attrs: { disabled: !_vm.valid, color: "success" },
                  on: { click: _vm.submit }
                },
                [_vm._v("\n                Update\n            ")]
              ),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  staticClass: "mr-4",
                  attrs: { color: "error" },
                  on: { click: _vm.reset }
                },
                [_vm._v("Clean form")]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/modules/orders/pages/OrderEditForm.vue":
/*!*************************************************************!*\
  !*** ./resources/js/modules/orders/pages/OrderEditForm.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _OrderEditForm_vue_vue_type_template_id_e13375e6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./OrderEditForm.vue?vue&type=template&id=e13375e6& */ "./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=template&id=e13375e6&");
/* harmony import */ var _OrderEditForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OrderEditForm.vue?vue&type=script&lang=js& */ "./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _OrderEditForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _OrderEditForm_vue_vue_type_template_id_e13375e6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _OrderEditForm_vue_vue_type_template_id_e13375e6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/modules/orders/pages/OrderEditForm.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OrderEditForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./OrderEditForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OrderEditForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=template&id=e13375e6&":
/*!********************************************************************************************!*\
  !*** ./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=template&id=e13375e6& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OrderEditForm_vue_vue_type_template_id_e13375e6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./OrderEditForm.vue?vue&type=template&id=e13375e6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/orders/pages/OrderEditForm.vue?vue&type=template&id=e13375e6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OrderEditForm_vue_vue_type_template_id_e13375e6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OrderEditForm_vue_vue_type_template_id_e13375e6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);